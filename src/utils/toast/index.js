import {ToastAndroid} from 'react-native';

export const showToast = val => {
  ToastAndroid.show(val, ToastAndroid.SHORT);
};
