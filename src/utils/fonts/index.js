export const fonts = {
  medium: 'Montserrat-Medium',
  regular: 'Montserrat-Regular',
  bold: 'Montserrat-Bold',
  thin: 'Montserrat-Thin',
};
