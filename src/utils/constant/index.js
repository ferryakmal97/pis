import {Dimensions} from 'react-native';

export const heightMobileUI = 896;
export const widthMobileUI = 414;

export const {height, width} = Dimensions.get('window');

export const API_LOGIN = 'https://tasklogin.herokuapp.com/api/login';
export const API_CONTACT = 'https://simple-contact-crud.herokuapp.com/contact';
