export const colors = {
  primary: '#22668A',
  white: '#FFFFFF',
  blue: '#6AB1D7',
  yellow: '#FFF6D5',
  border: '#C4C4C4',
  gold: '#FFC700',
  black: '#020202',
  grey: '#8D92A3',
  midGrey: '#F0F0F0',
  tosca: '#1ABC9C',
  red: '#D9435E',
};
