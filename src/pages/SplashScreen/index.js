import React, {Component} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {getData} from '../../utils';

class SplashScreen extends Component {
  componentDidMount() {
    setTimeout(async () => {
      const token = await getData('contact');
      this.props.navigation.replace(token ? 'Home' : 'Login');
    }, 500);
  }

  render() {
    return (
      <View style={styles.container}>
        <Text> Loading . . . </Text>
      </View>
    );
  }
}

export default SplashScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
