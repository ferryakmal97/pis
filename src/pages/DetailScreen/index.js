import axios from 'axios';
import React, {Component} from 'react';
import {
  Image,
  ImageBackground,
  ScrollView,
  StyleSheet,
  View,
} from 'react-native';
import {user} from '../../assets/image';
import {CButton, CGap, CTextInput} from '../../component';
import {API_CONTACT, colors, showToast, width} from '../../utils';

class DetailScreen extends Component {
  constructor(props) {
    super(props);
    const {firstName, lastName, age, photo} = this.props.route.params;
    this.state = {
      firstName,
      lastName,
      photo,
      age: age.toString(),
      isLoading: false,
    };
  }

  _handleChange = (type, value) => {
    this.setState({
      [type]: value,
    });
  };

  _onUpdate = async () => {
    const {firstName, lastName, age, id} = this.state;
    const {navigation} = this.props;
    if (firstName && lastName && age) {
      this.setState({isLoading: true});
      const newAge = Number(age);
      const data = {
        firstName,
        lastName,
        age: newAge,
      };
      axios
        .put(`${API_CONTACT}/${id}`, data)
        .then(ress => {
          console.log(ress);
          this.setState({onEdit: false});
          this._getUsers();
          showToast('User Successfully Updated !!!');
          navigation.replace('Home');
        })
        .catch(err => {
          console.log(err);
          showToast('User Unsuccessfully Updated !!!');
        })
        .finally(() => {
          this.setState({isLoading: false});
        });
    } else {
      showToast('Please Input Form Correctly !!!');
    }
  };

  render() {
    const {firstName, lastName, age, isLoading, photo} = this.state;
    return (
      <ScrollView style={styles.container}>
        <ImageBackground
          source={photo !== 'N/A' ? {uri: photo} : user}
          style={styles.imgCont}
          blurRadius={50}>
          <Image
            source={photo !== 'N/A' ? {uri: photo} : user}
            style={styles.img}
          />
        </ImageBackground>
        <View style={styles.formContainer}>
          <CTextInput
            placeholder="Input Your First Name"
            onChangeText={val => this._handleChange('firstName', val)}
            value={firstName}
            title="First Name"
          />
          <CGap height={12} />
          <CTextInput
            placeholder="Input Your Last Name"
            onChangeText={val => this._handleChange('lastName', val)}
            value={lastName}
            title="Last Name"
          />
          <CGap height={12} />
          <CTextInput
            placeholder="Input Your Age"
            onChangeText={val => this._handleChange('age', val)}
            value={age}
            title="Age"
            keyboardType="numeric"
          />
          <CGap height={20} />
          <CButton
            isLoading={isLoading}
            text="Update"
            color={colors.gold}
            onPress={this._onUpdate}
          />
        </View>
      </ScrollView>
    );
  }
}

export default DetailScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.white,
  },
  formContainer: {
    padding: 10,
  },
  imgCont: {
    width: width,
    height: width - (width * 25) / 100,
    alignItems: 'center',
    justifyContent: 'center',
  },
  img: {
    width: width - (width * 50) / 100,
    height: width - (width * 50) / 100,
    borderRadius: (width - (width * 50) / 100) / 2,
  },
});
