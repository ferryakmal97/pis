import axios from 'axios';
import React, {Component} from 'react';
import {StyleSheet, Text, ToastAndroid, View} from 'react-native';
import {CButton, CGap, CTextInput} from '../../component';
import {colors} from '../../styles';
import {API_LOGIN, showToast, storeData} from '../../utils';
import AsyncStorage from '@react-native-async-storage/async-storage';

class LoginScreen extends Component {
  constructor() {
    super();
    this.state = {
      username: '',
      password: '',
      isLoading: false,
    };
  }

  onSubmit = () => {
    const {username, password} = this.state;
    if (username && password) {
      this.setState({isLoading: true});
      axios
        .post(API_LOGIN, this.state)
        .then(result => {
          storeData('token', result.data.access_token);
          showToast('Login Success');
        })
        .then(() => {
          this.props.navigation.replace('Home');
        })
        .catch(err => {
          console.log(err);
          showToast('Login Unsuccessfully');
        })
        .finally(() => this.setState({isLoading: false}));
    } else {
      showToast('Please Input Form Correctly !');
    }
  };

  handleChange = (type, val) => {
    this.setState({
      [type]: val,
    });
  };

  render() {
    const {username, password, isLoading} = this.state;
    return (
      <View style={styles.container}>
        <CTextInput
          value={username}
          title="Username"
          placeholder="Type your username"
          onChangeText={val => this.handleChange('username', val)}
        />
        <CGap height={12} />
        <CTextInput
          value={password}
          title="Password"
          placeholder="Type your password"
          onChangeText={val => this.handleChange('password', val)}
          secureTextEntry
        />
        <CGap height={20} />
        <CButton isLoading={isLoading} text="Sign In" onPress={this.onSubmit} />
      </View>
    );
  }
}

export default LoginScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    paddingHorizontal: 18,
    backgroundColor: colors.white,
  },
});
