import axios from 'axios';
import React, {Component} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {CButton, CGap, CHeader, CTextInput} from '../../component';
import {colors} from '../../styles';
import {API_CONTACT, showToast} from '../../utils';

class AddScreen extends Component {
  constructor() {
    super();
    this.state = {
      firstName: '',
      lastName: '',
      age: '',
      isLoading: false,
    };
  }

  _handleChange = (type, value) => {
    this.setState({
      [type]: value,
    });
  };

  _onSubmit = () => {
    const {firstName, lastName, age, isLoading} = this.state;
    if (firstName && lastName && age) {
      this.setState({isLoading: true});
      const newAge = Number(age);
      const data = {
        firstName,
        lastName,
        age: newAge,
        photo:
          'https://img.okezone.com/content/2015/07/24/45/1184706/liverpool-terus-kejar-pengganti-sterling-SrZpQRncCp.jpg',
      };
      axios
        .post(API_CONTACT, data)
        .then(ress => {
          console.log(ress);
          this.setState({firstName: '', lastName: '', age: ''});
          showToast('User Successfully Added !!!');
          setTimeout(() => {
            this.props.navigation.reset({
              index: 0,
              routes: [{name: 'Home'}],
            });
          }, 1000);
        })
        .catch(err => {
          console.log(err);
          showToast('User Unsuccessfully Added !!!');
        })
        .finally(() => {
          this.setState({isLoading: false});
        });
    } else {
      showToast('Please Input Form Correctly !!!');
    }
  };

  render() {
    const {firstName, lastName, age, isLoading} = this.state;
    return (
      <View style={styles.container}>
        <CHeader text="Add User" />
        <View style={styles.formContainer}>
          <CTextInput
            placeholder="Input Your First Name"
            onChangeText={val => this._handleChange('firstName', val)}
            value={firstName}
            title="First Name"
          />
          <CGap height={12} />
          <CTextInput
            placeholder="Input Your Last Name"
            onChangeText={val => this._handleChange('lastName', val)}
            value={lastName}
            title="Last Name"
          />
          <CGap height={12} />
          <CTextInput
            placeholder="Input Your Age"
            onChangeText={val => this._handleChange('age', val)}
            value={age}
            title="Age"
            keyboardType="numeric"
          />
          <CGap height={20} />
          <CButton
            isLoading={isLoading}
            text="Submit"
            onPress={this._onSubmit}
          />
        </View>
      </View>
    );
  }
}

export default AddScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.white,
  },
  formContainer: {
    padding: 10,
  },
});
