import SplashScreen from './SplashScreen';
import LoginScreen from './LoginScreen';
import HomeScreen from './HomeScreen';
import DetailScreen from './DetailScreen';
import AddScreen from './AddScreen';

export {SplashScreen, LoginScreen, HomeScreen, DetailScreen, AddScreen};
