import React, {Component} from 'react';
import axios from 'axios';
import {
  View,
  StyleSheet,
  FlatList,
  Text,
  ActivityIndicator,
} from 'react-native';
import {CHeader, CListItem} from '../../component';
import {
  API_CONTACT,
  clearStorage,
  colors,
  fonts,
  getData,
  showToast,
  storeData,
} from '../../utils';

class HomeScreen extends Component {
  constructor() {
    super();
    this.state = {
      data: [],
      isLoading: true,
    };
  }

  async componentDidMount() {
    await this._getData();
    this._syncContact();
  }

  _getData = async () => {
    const dataLocal = await getData('contact');

    this.setState({
      isLoading: false,
      data: dataLocal ? dataLocal : [],
    });
  };

  _syncContact = async () => {
    try {
      const result = await axios.get(API_CONTACT);
      if (
        JSON.stringify(result.data.data) !== JSON.stringify(this.state.data)
      ) {
        storeData('contact', result.data.data);
        this._getData();
      } else {
        this._getData();
      }
    } catch (error) {
      console.log(error);
      showToast('Something gone wrong !');
    }
  };

  _onDelete = id => {
    axios
      .delete(`${API_CONTACT}/${id}`)
      .then(ress => {
        showToast('User Deleted Successfully !');
        this._syncContact();
      })
      .catch(err => {
        console.log(err);
        showToast('Failed to delete user !');
      });
  };

  _onRefresh = () => {
    this.setState({isLoading: true});
    this._syncContact();
  };

  _signOut = () => {
    clearStorage().then(() => {
      showToast('Please Login If You Want To Use This App');
      this.props.navigation.replace('Login');
    });
  };

  render() {
    const {data, isLoading} = this.state;
    const {navigation} = this.props;

    const renderData = () => {
      return data.length > 0 ? (
        <FlatList
          data={data}
          keyExtractor={item => item.id}
          renderItem={({item}) => (
            <CListItem
              data={item}
              onDelete={() => this._onDelete(item.id)}
              onPress={() => navigation.navigate('Detail', item)}
            />
          )}
          onRefresh={() => this._onRefresh()}
          refreshing={isLoading}
        />
      ) : (
        <Text style={styles.noData}>NO CONTACT . . . </Text>
      );
    };

    return (
      <View style={styles.container}>
        <CHeader
          text="H O M E"
          onAdd={() => navigation.navigate('Add')}
          onLogOut={this._signOut}
        />
        {isLoading ? (
          <ActivityIndicator
            size="large"
            color={colors.primary}
            style={styles.loading}
          />
        ) : (
          renderData()
        )}
      </View>
    );
  }
}

export default HomeScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  buttonContainer: {
    alignItems: 'center',
    paddingVertical: 20,
    bottom: 0,
    width: '100%',
  },
  noData: {
    textAlign: 'center',
    textAlignVertical: 'center',
    fontSize: 20,
    fontFamily: fonts.medium,
    color: colors.grey,
    flex: 1,
  },
  loading: {
    flex: 1,
  },
});
