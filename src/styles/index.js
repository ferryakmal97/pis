export const fonts = {
  medium: 'Montserrat-Medium',
  regular: 'Montserrat-Regular',
  bold: 'Montserrat-Bold',
  thin: 'Montserrat-Thin',
};

export const colors = {
  yellow: '#FFC700',
  black: '#020202',
  grey: '#8D92A3',
  white: 'white',
  midGrey: '#F0F0F0',
  tosca: '#1ABC9C',
  red: '#D9435E',
};
