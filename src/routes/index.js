import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import {
  AddScreen,
  DetailScreen,
  HomeScreen,
  LoginScreen,
  SplashScreen,
} from '../pages';

const Stack = createNativeStackNavigator();

const Hide = {headerShown: false};

const Routes = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Splash">
        <Stack.Screen name="Splash" component={SplashScreen} options={Hide} />
        <Stack.Screen name="Login" component={LoginScreen} options={Hide} />
        <Stack.Screen name="Home" component={HomeScreen} options={Hide} />
        <Stack.Screen name="Add" component={AddScreen} options={Hide} />
        <Stack.Screen name="Detail" component={DetailScreen} options={Hide} />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default Routes;
