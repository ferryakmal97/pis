import React from 'react';
import {View, Text, TouchableOpacity, StyleSheet, Image} from 'react-native';
import {colors, fonts} from '../../styles';
import {user} from '../../assets/image';
import AntIcon from 'react-native-vector-icons/AntDesign';

const CListItem = ({data, onDelete, onPress}) => {
  const {firstName, lastName, age, photo} = data;
  return (
    <TouchableOpacity
      activeOpacity={0.6}
      style={styles.container}
      onPress={onPress}>
      <Image
        source={photo !== 'N/A' ? {uri: photo} : user}
        style={styles.img}
      />
      <View style={styles.detail}>
        <Text style={styles.name}>
          {firstName} {lastName}
        </Text>
        <Text style={styles.age}>{age}</Text>
      </View>
      <TouchableOpacity
        activeOpacity={0.6}
        style={styles.iconCont}
        onPress={onDelete}>
        <AntIcon name="deleteuser" size={26} color={colors.white} />
      </TouchableOpacity>
    </TouchableOpacity>
  );
};

export default CListItem;

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.white,
    padding: 10,
    flexDirection: 'row',
    alignItems: 'center',
    borderBottomWidth: 1,
    borderBottomColor: colors.grey,
  },
  img: {
    width: 50,
    height: 50,
    borderRadius: 50 / 2,
  },
  detail: {
    flex: 1,
    marginHorizontal: 10,
  },
  name: {
    fontSize: 20,
    fontFamily: fonts.medium,
    color: colors.black,
  },
  age: {
    fontSize: 14,
    fontFamily: fonts.regular,
    color: colors.black,
  },
  iconCont: {
    backgroundColor: colors.red,
    alignItems: 'center',
    justifyContent: 'center',
    width: 40,
    height: 40,
    borderRadius: 14,
    elevation: 2,
  },
});
