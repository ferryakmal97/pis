import React, {memo} from 'react';
import {View} from 'react-native';

const CGap = ({height, width}) => {
  return <View style={{width, height}} />;
};

export default memo(CGap);
