import React from 'react';
import {
  ActivityIndicator,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {colors, fonts} from '../../styles';

const CButton = ({
  text,
  color = colors.yellow,
  textColor = colors.black,
  onPress,
  isLoading,
  ...restProps
}) => {
  return (
    <TouchableOpacity
      activeOpacity={0.7}
      onPress={onPress}
      style={styles.container(isLoading ? colors.midGrey : color)}
      disabled={isLoading}
      {...restProps}>
      {isLoading ? (
        <ActivityIndicator color={textColor} />
      ) : (
        <Text style={styles.text(textColor)}>{text}</Text>
      )}
    </TouchableOpacity>
  );
};

export default CButton;

const styles = StyleSheet.create({
  container: color => ({
    backgroundColor: color,
    padding: 12,
    borderRadius: 8,
    elevation: 2,
  }),
  text: textColor => ({
    fontSize: 14,
    color: textColor,
    textAlign: 'center',
    fontFamily: fonts.medium,
  }),
});
