import React from 'react';
import {View, Text, StyleSheet, TouchableOpacity} from 'react-native';
import {colors, fonts} from '../../styles';
import AntIcon from 'react-native-vector-icons/AntDesign';
import {responsiveHeight} from '../../utils';

const CHeader = ({text = 'Header', onAdd, onLogOut}) => {
  return (
    <View style={styles.container}>
      {onLogOut && (
        <TouchableOpacity
          activeOpacity={0.6}
          style={styles.iconCont}
          onPress={onLogOut}>
          <AntIcon name="logout" size={26} color={colors.white} />
        </TouchableOpacity>
      )}
      <Text style={styles.text}>{text.toUpperCase()}</Text>
      {onAdd && (
        <TouchableOpacity
          activeOpacity={0.6}
          style={styles.iconCont}
          onPress={onAdd}>
          <AntIcon name="adduser" size={26} color={colors.white} />
        </TouchableOpacity>
      )}
    </View>
  );
};

export default CHeader;

const styles = StyleSheet.create({
  container: {
    height: responsiveHeight(55),
    backgroundColor: colors.black,
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'space-between',
    padding: 10,
  },
  text: {
    color: colors.white,
    fontSize: 20,
    textAlign: 'center',
    fontFamily: fonts.medium,
  },
  iconCont: {
    alignItems: 'center',
    justifyContent: 'center',
    width: 45,
    height: 45,
    borderRadius: 14,
  },
});
