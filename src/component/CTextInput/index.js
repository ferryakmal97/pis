import React from 'react';
import {View, Text, StyleSheet, TextInput} from 'react-native';
import {CGap} from '..';
import {colors, fonts} from '../../styles';

const CTextInput = ({
  title = 'Title',
  placeholder = 'Placeholder',
  value,
  ...restProps
}) => {
  return (
    <View style={styles.container}>
      <Text style={styles.title}>{title}</Text>
      <CGap height={8} />
      <TextInput
        value={value}
        placeholder={placeholder}
        style={styles.textinput}
        {...restProps}
      />
    </View>
  );
};

export default CTextInput;

const styles = StyleSheet.create({
  container: {
    width: '100%',
  },
  title: {
    fontFamily: fonts.medium,
    fontSize: 16,
    color: colors.black,
  },
  textinput: {
    borderWidth: 0.8,
    borderRadius: 8,
    backgroundColor: 'white',
    elevation: 2,
    fontFamily: fonts.regular,
    paddingHorizontal: 12,
    borderColor: colors.grey,
  },
});
